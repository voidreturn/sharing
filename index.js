const express = require('express');
const app = express();
const client = require('twilio')(process.env.TWILIO_SID, process.env.TWILIO_TOKEN);
const bip39 = require('bip39');
const mongoose = require('mongoose');

const Receiver = mongoose.model('Receiver', new mongoose.Schema({
  number: String,
  mnemonic: String,
}, {
  collection: 'receivers',
}));

app.use(express.json());

// Wrap an async function in a promise for express consumption
const asyncHandler = (fn) => (req, res, next) => fn(req, res, next).then(next).catch(next);

app.use(asyncHandler(async () => {
  await mongoose.connect(process.env.DB_URI, {
    connectTimeoutMS: 5000,
    useNewUrlParser: true,
  });
}));

app.get('/', asyncHandler(async (req, res) => {
  if (req.query.secret !== process.env.SECRET) {
    res.status(401);
    res.send('Missing or incorrect secret supplied');
    return;
  }
  const normalizedNumber = req.query.number.replace(/\D/g, '');
  const phoneRegex = /\d{11,11}/;
  if (!phoneRegex.test(normalizedNumber)) {
    res.status(400);
    res.send('Invalid phone number, must be 11 numbers');
    return;
  }
  const doc = await Receiver.findOne({
    number: normalizedNumber
  }).lean().exec();
  if (doc) throw new Error('Number already present');

  const mnemonic = bip39.generateMnemonic();
  await Receiver.create({
    number: normalizedNumber,
    mnemonic,
  });
  await client.messages.create({
    body: mnemonic,
    from: process.env.TWILIO_NUMBER,
    to: normalizedNumber,
  });
  await client.messages.create({
    body: 'https://gitlab.com/voidreturn/sense#readme',
    from: process.env.TWILIO_NUMBER,
    to: normalizedNumber,
  });
  res.status(204);
  res.end();
}));

module.exports = app;
